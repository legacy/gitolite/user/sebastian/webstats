#!/bin/bash

set -u
set -e

BASEDIR=/srv/webstats.torproject.org/
SCRIPTDIR="${BASEDIR}/bin/"

BASEINCOMINGDIR="${BASEDIR}/incoming/"

INTERESTING_HOSTS="www.torproject.org dist.torproject.org"

cd "${BASEINCOMINGDIR}"
for host in *; do
    INCOMINGDIR="${BASEINCOMINGDIR}/${host}/"
    WORKDIR="${BASEDIR}/work/${host}/"
    WORKDIR_AWSTATS="${BASEDIR}/work_awstats/${host}/"
    OUTDIR="${BASEDIR}/out/${host}/"
    STAMPDIR="${BASEDIR}/stamp/${host}/"

    cd "${INCOMINGDIR}"
    mkdir -p "${WORKDIR}"
    mkdir -p "${WORKDIR_AWSTATS}"
    mkdir -p "${OUTDIR}"
    mkdir -p "${STAMPDIR}"

    for file in *; do
        basefile=${file%.gz}
        if [ -e "${STAMPDIR}/${file}_treated" ]; then
            continue
        fi
        cp "${INCOMINGDIR}/${file}" "${WORKDIR}/${file}"
        cd "${WORKDIR}"
        gunzip ${file}
        COMPLETED=$(${SCRIPTDIR}/sanitize.py "${basefile}" "${WORKDIR}" 2>>"${WORKDIR}/errors")
        COMPLETED_BASE=$(basename $COMPLETED)
        COMPLETED_BASE=${COMPLETED_BASE%_sanitized}
        sort "${COMPLETED}" > "${COMPLETED}_sorted"
        xz -ck9e "${COMPLETED}_sorted" > "${OUTDIR}/${COMPLETED_BASE}.xz"
        cp "${OUTDIR}/${COMPLETED_BASE}.xz" "${WORKDIR_AWSTATS}"

        rm "${WORKDIR}/${basefile}"
        rm "${COMPLETED}_sorted"
        rm "${WORKDIR}/${COMPLETED_BASE}_sanitized"
        touch "${STAMPDIR}/${file}_treated"
    done
done

# Now that we have all output files, process them with awstats

