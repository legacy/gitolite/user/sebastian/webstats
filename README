Webstats: Code and website to sanitize and visualize .tpo Apache web logs
=========================================================================

1  Copying access.log files
===========================

1.1  Preparing the source host
==============================

Configure Apache logging and log rotation.  The following three
assumptions must be met:

  a. Log files with file names ending in .gz never change after creation.

  b. Log file names ending in .gz are unique per server even across log
     file directories.

  c. Log file names must not contain spaces.

From this follows that a virtual host name should be part of the file
name and that compressed log files should contain a date as part of the
file name.

A good log file name would be, for example,

  www.torproject.org-access.log-20111224.gz

A bad log file name would be, for example,

  access.log.16.gz

Copy bin/send-log.sh to the host sending log files.

Create an SSH key pair for every host (not for every *virtual* host) that
is supposed to send its web logs to the analysis server.


1.2  Preparing the destination host
===================================

Add the public key to .ssh/authorized_keys on the analysis server.  Limit
the key to a single command and possibly to a single IP address or domain
name.  (Replace $domainname in the line below with the domain name and
$hostname with a name that will later appear in the path of received
files.)

  from="$domainname",command="cd webstats && bin/receive-log.sh $hostname"

Further lock down the SSH key on the destination host by adding the
following options to the authorized_keys line:

  no-port-forwarding,no-X11-forwarding,no-agent-forwarding,no-pty


1.3  Enabling log-file copying
==============================

Run the copying script manually for the first time.  The usage line is:

  Usage: ./send-logs.sh logs-dir ssh-string state-dir

  a. logs-dir is the local directory containing web log files.  No changes
     will be made to this directory by the copying script.  This could be,
     for example, "/var/log/apache2".

  b. ssh-string is the SSH string to connect to the destination host.  An
     example ssh-string would be (including double quotes):

     "-i ~/.ssh/from-majus webstats@stenodon.torproject.org"

  c. state-dir is a local directory that contains file names of log files
     that have been copied before.  An example would be "sent-logs".

The full example command would be:

  $ ./send-logs.sh /var/log/apache2 \
     "-i ~/.ssh/from-majus webstats@stenodon.torproject.org" sent-logs

Once this command runs without issues, add a crontab line for it.

2. Setting up Webalizer to process logs for a new host
======================================================

The first thing you need to do is create an output directory for the
host, a configuration file, and a directory for the tarballs. Here's an
example:

  $ cd /srv/webstats.torproject.org
  $ mkdir htdocs/webalizer/metrics.torproject.org
  $ cp configs/webalizer.www.torproject.org.conf configs/webalizer.metrics.torproject.org.conf
  $ mkdir to-archive/metrics.torproject.org.conf 

Remember to edit line 1 (LogFile), 2 (OutputDir), and 3 (HostName) in the new
configuration file.

Edit logimport.sh and logarchive.sh to include the name of the new host in the hosts-section.
